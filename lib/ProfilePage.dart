import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter/src/widgets/framework.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Proflie",
                    style: TextStyle(fontSize: 24),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
          ),
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child: Icon(
              Icons.account_circle,
              size: 90,
              color: Colors.amber,
            ),
          ),
          Container(
            child: Column(children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "ประวัตินิสิต",
                  textScaleFactor: 2,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Table(
                  children: [
                    TableRow(
                      children: [
                        Text("ประวัติส่วนตัว"),
                        Text(""),
                      ],
                    ),
                    TableRow(children: [
                      Text("ชื่อ"),
                      Text("รัตนาลักษณ์ จิตจักร์"),
                    ]),
                    TableRow(children: [
                      Text("รหัสนิสิต"),
                      Text("63160079"),
                    ]),
                    TableRow(children: [
                      Text("คณะ"),
                      Text("color: Colors.blue,"),
                    ]),
                    TableRow(children: [
                      Text("อ. ที่ปรึกษา"),
                      Text(
                          "	ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม"),
                    ]),
                    TableRow(children: [
                      Text("(ผลการศึกษา"),
                      Text(""),
                    ]),
                    TableRow(children: [
                      Text("เกรดเฉลี่ยสะสม"),
                      Text("3.39"),
                    ]),
                  ],
                ),
              ),
            ]),
          ),
          Container(
            child: IconButton(
              icon: Icon(
                Icons.exit_to_app,
                color: Colors.red,
                size: 50,
              ),
              onPressed: () {},
            ),
          ),
        ],
      ),
    ],
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
    title: Text('Burapha University'),
    backgroundColor: Colors.grey,
  );
}
