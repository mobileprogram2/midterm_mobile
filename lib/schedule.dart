import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class SchedulePage extends StatelessWidget {
  const SchedulePage({super.key});

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

Widget build(BuildContext context) {
  return MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: buildAppBarWidget(),
      body: buildBodyWidget(),
    ),
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Schedule",
                    style: TextStyle(fontSize: 24),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
          ),
          Container(
            child: Column(children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "ตารางเรียนภาคเรียนที่1/2565",
                  textScaleFactor: 2,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Table(
                  children: [
                    TableRow(children: [
                      Text("Day/Time"),
                      Text("10.00-11.00"),
                      Text("11.00-12.00"),
                      Text("12.00-13.00"),
                      Text("14.00-15.00"),
                      Text("15.00-16.00"),
                      Text("16.00-17.00"),
                      Text("18.00-19.00"),
                    ]),
                    TableRow(children: [
                      Text("จันทร์"),
                      Text(""),
                      Text("88624259"),
                      Text(""),
                      Text(""),
                      Text(""),
                      Text(""),
                      Text(""),
                    ]),
                    TableRow(children: [
                      Text("อังคาร"),
                      Text(""),
                      Text("88624259"),
                      Text(""),
                      Text(""),
                      Text(""),
                      Text(""),
                      Text(""),
                    ]),
                    TableRow(children: [
                      Text("พุธ"),
                      Text(""),
                      Text("88624259"),
                      Text(""),
                      Text(""),
                      Text("88634159"),
                      Text(""),
                      Text(""),
                    ]),
                    TableRow(children: [
                      Text("พฤหัสบดี"),
                      Text(""),
                      Text(""),
                      Text(""),
                      Text(""),
                      Text(""),
                      Text(""),
                      Text("88634159")
                    ]),
                    TableRow(children: [
                      Text("ศุกร์"),
                      Text(""),
                      Text("88624259"),
                      Text(""),
                      Text("88633159"),
                      Text(""),
                      Text("88634159"),
                      Text(""),
                    ]),
                  ],
                ),
              ),
            ]),
          ),
          Container(
            child: Column(children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "ตารางสอบ",
                  textScaleFactor: 2,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Table(
                  children: [
                    TableRow(children: [
                      Text("ลำดับ"),
                      Text("รหัสวิชา"),
                      Text("รายวิชา"),
                      Text("หน่วยกิต"),
                      Text("กลางภาค"),
                      Text("ปลายภาค")
                    ]),
                    TableRow(children: [
                      Text("1"),
                      Text("88624259"),
                      Text("MobilmMobe Programming Paradige"),
                      Text("3"),
                      Text("-"),
                      Text("-")
                    ]),
                    TableRow(children: [
                      Text("2"),
                      Text("88624259"),
                      Text("Algorithm Design and ApplicationsWeb Programing"),
                      Text("3"),
                      Text("-"),
                      Text("-")
                    ]),
                    TableRow(children: [
                      Text("3"),
                      Text("88633159"),
                      Text("Computer Networks"),
                      Text("3"),
                      Text("-"),
                      Text("-")
                    ]),
                    TableRow(children: [
                      Text("4"),
                      Text("88634159"),
                      Text("Software Development"),
                      Text("3"),
                      Text("-"),
                      Text("-")
                    ]),
                    TableRow(children: [
                      Text("5"),
                      Text("88634159"),
                      Text("User Interface Design and Development"),
                      Text("3"),
                      Text("-"),
                      Text("-")
                    ]),
                    TableRow(children: [
                      Text("6"),
                      Text(" 88636159"),
                      Text("Introduction to Artificial Intelligence"),
                      Text("3"),
                      Text("-"),
                      Text("-")
                    ]),
                  ],
                ),
              ),
            ]),
          ),
        ],
      ),
    ],
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
    title: Text('Burapha University'),
    backgroundColor: Colors.grey,
  );
}
