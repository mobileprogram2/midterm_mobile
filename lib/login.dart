import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:reg_midterm/menu.dart';

void main() {
  runApp(BaseLayout());
}


class BaseLayout extends StatelessWidget {
  double rowPadding = 24.0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.grey,
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Text(
                  "ยินดีต้อนรับสู่มหาวิทยาลัยบูรพา",
                  style: TextStyle(
                    fontSize: 60,
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                  ),
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              const Text(
                "Welcome to Burapha University",
                style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w300,
                    color: Colors.black),
              ),
              const SizedBox(
                height: 40.0,
              ),
              Center(
                child: ConstrainedBox(
                  constraints: const BoxConstraints(maxWidth: 300.0),
                  child: Column(
                    children: [
                      const SizedBox(height: 80.0),
                      const Padding(
                          padding: EdgeInsets.symmetric(vertical: 10.0),
                          child: Icon(
                            Icons.account_circle, size: 90, color: Colors.black,
                            // color: Colors.indigo.shade800,
                          ),
                      ),
                      const SizedBox(height: 20.0),
                      const TextField(
                          decoration: InputDecoration(
                        labelText: 'Username',
                      )),
                      const TextField(
                          decoration: InputDecoration(labelText: 'Password')),
                      const SizedBox(height: 30.0),
                      ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => const MenuPage()));
                          },
                          child: const Text('Login')),
                    ],
                  ),
                ),
              ),
            ],
          ), /* add child content here */
        ),
      ),
    );
  }
}
