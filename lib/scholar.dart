import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class ScholarPage extends StatelessWidget {
  const ScholarPage({super.key});

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

Widget build(BuildContext context) {
  return MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: buildAppBarWidget(),
      body: buildBodyWidget(),
    ),
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Scholar",
                    style: TextStyle(fontSize: 24),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
          ),
          Container(
            child: Column(children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "ภาคเรียนที่1/2565",
                  textScaleFactor: 2,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Table(
                  children: [
                    TableRow(children: [
                      Text("ลำดับ"),
                      Text("รหัสวิชา"),
                      Text("รายวิชา"),
                      Text("หน่วยกิต"),
                      Text("เกรด"),
                    ]),
                    TableRow(children: [
                      Text("1"),
                      Text("88624259"),
                      Text("Mobile Programming ParadigmMobile Programing"),
                      Text("3"),
                      Text("A"),
                    ]),
                    TableRow(children: [
                      Text("2"),
                      Text("88624259"),
                      Text("Algorithm Design and ApplicationsWeb Programing"),
                      Text("3"),
                      Text("B+"),
                    ]),
                    TableRow(children: [
                      Text("3"),
                      Text("88633159"),
                      Text("Computer Networks"),
                      Text("3"),
                      Text("A"),
                    ]),
                    TableRow(children: [
                      Text("4"),
                      Text("88634159"),
                      Text("Software Development"),
                      Text("3"),
                      Text("A"),
                    ]),
                    TableRow(children: [
                      Text("5"),
                      Text("88634159"),
                      Text("User Interface Design and Development"),
                      Text("3"),
                      Text("A"),
                    ]),
                    TableRow(children: [
                      Text("6"),
                      Text(" 88636159"),
                      Text("Introduction to Artificial Intelligence"),
                      Text("3"),
                      Text("A"),
                    ]),
                  ],
                ),
              ),
            ]),
          ),
          Container(
            child: Column(children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "ภาคเรียนที่2/2565",
                  textScaleFactor: 2,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Table(
                  children: [
                    TableRow(children: [
                      Text("ลำดับ"),
                      Text("รหัสวิชา"),
                      Text("รายวิชา"),
                      Text("หน่วยกิต"),
                      Text("เกรด"),
                    ]),
                    TableRow(children: [
                      Text("1"),
                      Text("88624259"),
                      Text("Mobile Programming ParadigmMobile Programing"),
                      Text("3"),
                      Text("A"),
                    ]),
                    TableRow(children: [
                      Text("2"),
                      Text("88624259"),
                      Text("Algorithm Design and ApplicationsWeb Programing"),
                      Text("3"),
                      Text("B+"),
                    ]),
                    TableRow(children: [
                      Text("3"),
                      Text("88633159"),
                      Text("Computer Networks"),
                      Text("3"),
                      Text("A"),
                    ]),
                    TableRow(children: [
                      Text("4"),
                      Text("88634159"),
                      Text("Software Development"),
                      Text("3"),
                      Text("A"),
                    ]),
                    TableRow(children: [
                      Text("5"),
                      Text("88634159"),
                      Text("User Interface Design and Development"),
                      Text("3"),
                      Text("A"),
                    ]),
                    TableRow(children: [
                      Text("6"),
                      Text(" 88636159"),
                      Text("Introduction to Artificial Intelligence"),
                      Text("3"),
                      Text("A"),
                    ]),
                  ],
                ),
              ),
            ]),
          ),
        ],
      ),
    ],
  );
}

AppBar buildAppBarWidget() {
  return AppBar(
    title: Text('Burapha University'),
    backgroundColor: Colors.grey,
  );
}
